package day07

import (
	"fmt"
	"testing"

	"gitlab.com/rlbekkema/advent-of-code-2018/utils"
)

func TestPuzzle1(t *testing.T) {
	tt := []struct {
		lines  []string
		result string
	}{
		{[]string{"Step C must be finished before step A can begin.",
			"Step C must be finished before step F can begin.",
			"Step A must be finished before step B can begin.",
			"Step A must be finished before step D can begin.",
			"Step B must be finished before step E can begin.",
			"Step D must be finished before step E can begin.",
			"Step F must be finished before step E can begin."}, "CABDFE"},
	}

	for idx, tc := range tt {
		t.Run(fmt.Sprintf("Case %d", idx), func(t *testing.T) {
			result := puzzle1(tc.lines)
			if result != tc.result {
				t.Errorf("expected %q; got %q", tc.result, result)
			}
		})
	}
}

func TestPuzzle2(t *testing.T) {
	tt := []struct {
		lines  []string
		result int
	}{
		{[]string{"Step C must be finished before step A can begin.",
			"Step C must be finished before step F can begin.",
			"Step A must be finished before step B can begin.",
			"Step A must be finished before step D can begin.",
			"Step B must be finished before step E can begin.",
			"Step D must be finished before step E can begin.",
			"Step F must be finished before step E can begin."}, 15},
	}

	for idx, tc := range tt {
		t.Run(fmt.Sprintf("Case %d", idx), func(t *testing.T) {
			result := puzzle2(tc.lines, 0, 2)
			if result != tc.result {
				t.Errorf("expected %d; got %d", tc.result, result)
			}
		})
	}
}

func TestPuzzle2Solution(t *testing.T) {
	lines := utils.ReadLines("input")
	expected := 755

	result := puzzle2(lines, 60, 5)
	if result != expected {
		t.Errorf("expected %d; got %d", expected, result)
	}
}
