package day07

import (
	"fmt"
	"sort"

	"gitlab.com/rlbekkema/advent-of-code-2018/utils"
)

func puzzle1(lines []string) string {
	deps := make(map[rune]map[rune]bool)
	for _, line := range lines {
		var dep, ben rune
		fmt.Sscanf(line, "Step %c must be finished before step %c can begin.", &dep, &ben)
		// fmt.Printf("Char %s has dep %s\n", string(ben), string(dep))

		if _, ok := deps[ben]; !ok {
			deps[ben] = make(map[rune]bool)
		}
		deps[ben][dep] = true

		// Also enter dep into deps
		if _, ok := deps[dep]; !ok {
			deps[dep] = make(map[rune]bool)
		}
	}

	var steps []rune
	for {
		var depFrees []rune
		for char, cDeps := range deps {
			if len(cDeps) == 0 {
				depFrees = append(depFrees, char)
			}
		}

		if len(depFrees) == 0 {
			break
		}

		sort.Slice(depFrees, func(i, j int) bool {
			return depFrees[i] < depFrees[j]
		})

		// Process step and remove it from map keys and dependencies
		dep := depFrees[0]
		steps = append(steps, dep)
		delete(deps, dep)
		for char := range deps {
			delete(deps[char], dep)
		}
	}

	return string(steps)
}

func Puzzle1() string {
	lines := utils.ReadLines("day07/input")
	return puzzle1(lines)
}

type step struct {
	deps      map[rune]bool
	remaining int
	reserved  bool
}

func newStep(name rune, minTime int) *step {
	return &step{
		deps:      make(map[rune]bool),
		remaining: minTime + (int(name) - 64),
		reserved:  false,
	}
}

func puzzle2(lines []string, minTime, nrWorkers int) int {
	steps := make(map[rune]*step)
	for _, line := range lines {
		var dep, ben rune
		fmt.Sscanf(line, "Step %c must be finished before step %c can begin.", &dep, &ben)
		// fmt.Printf("Char %s has dep %s\n", string(ben), string(dep))

		if _, ok := steps[ben]; !ok {
			steps[ben] = newStep(ben, minTime)
		}
		steps[ben].deps[dep] = true

		// Also enter dep into deps
		if _, ok := steps[dep]; !ok {
			steps[dep] = newStep(dep, minTime)
		}
	}

	workers := make([]rune, nrWorkers)

	var counter int
	for {
		if counter == 755 {
			fmt.Println("Almost!")
		}

		// Process tasks
		for idx := range workers {
			if workers[idx] == 0 {
				continue
			}
			step := steps[workers[idx]]
			step.remaining--

			if step.remaining == 0 {
				delete(steps, workers[idx])
				for char := range steps {
					delete(steps[char].deps, workers[idx])
				}

				workers[idx] = 0
			}
		}

		// Assign tasks
		for idx := range workers {
			if workers[idx] != 0 {
				continue
			}

			var depFrees []rune
			for char, step := range steps {
				if len(step.deps) == 0 && !step.reserved {
					depFrees = append(depFrees, char)
				}
			}

			sort.Slice(depFrees, func(i, j int) bool {
				return depFrees[i] < depFrees[j]
			})

			if len(depFrees) > 0 {
				workers[idx] = depFrees[0]
				steps[depFrees[0]].reserved = true
			}
		}

		if len(steps) == 0 {
			break
		}

		counter++
	}

	return counter
}

func Puzzle2() int {
	lines := utils.ReadLines("day07/input")
	return puzzle2(lines, 60, 5)
}
