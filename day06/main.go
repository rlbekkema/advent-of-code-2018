package day06

import (
	"fmt"
	"math"

	"gitlab.com/rlbekkema/advent-of-code-2018/utils"
	"golang.org/x/xerrors"
)

type coord struct{ x, y int }

func extractCoords(lines []string) []coord {
	coords := make([]coord, len(lines))
	for idx, line := range lines {
		var x, y int
		fmt.Sscanf(line, "%d, %d", &x, &y)
		coords[idx] = coord{x, y}
	}
	return coords
}

func puzzle1(lines []string) (int, error) {
	coords := extractCoords(lines)
	var minX, minY, maxX, maxY int
	for _, coord := range coords {
		if coord.x < minX || minX == 0 {
			minX = coord.x
		}
		if coord.x > maxX {
			maxX = coord.x
		}
		if coord.y < minY || minY == 0 {
			minY = coord.y
		}
		if coord.y > maxY {
			maxY = coord.y
		}
	}

	gridW := maxX - minX + 1
	gridH := maxY - minY + 1

	// Create matrix
	fmt.Printf("Grid size %dx%d\n", gridW, gridH)

	counters := make([]int, len(coords))
	coordMap := make(map[coord]bool)
	for idx, coord := range coords {
		coordMap[coord] = true
		// If coord is on the edge, mark it as infinite
		if coord.x-minX == 0 || coord.x-minX == gridW-1 || coord.y-minY == 0 || coord.y-minY == gridH-1 {
			counters[idx] = -1
		}
	}

	// For each location, determine distances too coords
	for x := 0; x < gridW; x++ {
		for y := 0; y < gridH; y++ {
			realX := x + minX
			realY := y + minY

			// Skip if this location is a coordinate
			if coordMap[coord{realX, realY}] {
				continue
			}

			var minDistance int
			var minIdx int
			var minCount int
			for idx, coord := range coords {
				deltaX := int(math.Abs(float64(realX - coord.x)))
				deltaY := int(math.Abs(float64(realY - coord.y)))
				distance := deltaX + deltaY
				if distance == minDistance && minDistance != 0 {
					minCount++
				}
				if distance < minDistance || minDistance == 0 {
					minDistance = distance
					minIdx = idx
					minCount = 1
				}
			}
			if minCount > 1 {
				continue
			}
			// If location is on the edge, the closest coordinate is infinite
			if x == 0 || x == gridW-1 || y == 0 || y == gridH-1 {
				counters[minIdx] = -1
			}
			if counters[minIdx] != -1 {
				counters[minIdx]++
			}
		}
	}

	maxCount := -1
	maxIdx := 0
	for idx, counter := range counters {
		if counter != -1 && (counter > maxCount) {
			maxCount = counter
			maxIdx = idx
		}
	}
	maxCount++ // Also count coordinate itself

	if maxCount == -1 {
		return 0, xerrors.New("could not find finite coords")
	}

	fmt.Printf("The coord with largest finite area is idx %d: %dx%d with size %d\n", maxIdx, coords[maxIdx].x, coords[maxIdx].y, maxCount)
	return maxCount, nil
}

func Puzzle1() (int, error) {
	lines := utils.ReadLines("day06/input")
	return puzzle1(lines)
}

func puzzle2(lines []string, under int) (int, error) {
	coords := extractCoords(lines)
	var minX, minY, maxX, maxY int
	for _, coord := range coords {
		if coord.x < minX || minX == 0 {
			minX = coord.x
		}
		if coord.x > maxX {
			maxX = coord.x
		}
		if coord.y < minY || minY == 0 {
			minY = coord.y
		}
		if coord.y > maxY {
			maxY = coord.y
		}
	}

	gridW := maxX - minX + 1
	gridH := maxY - minY + 1

	locsUnder10k := 0
	for x := 0; x < gridW; x++ {
		for y := 0; y < gridH; y++ {
			realX := x + minX
			realY := y + minY
			var sumDistance int
			for _, coord := range coords {
				deltaX := int(math.Abs(float64(realX - coord.x)))
				deltaY := int(math.Abs(float64(realY - coord.y)))
				distance := deltaX + deltaY

				sumDistance += distance
			}
			if sumDistance < under {
				locsUnder10k++
			}
		}
	}
	return locsUnder10k, nil
}

func Puzzle2() (int, error) {
	lines := utils.ReadLines("day06/input")
	return puzzle2(lines, 10000)
}
