package day06

import (
	"fmt"
	"testing"

	"gitlab.com/rlbekkema/advent-of-code-2018/utils"
)

func TestPuzzle1(t *testing.T) {
	tt := []struct {
		lines  []string
		result int
	}{
		{[]string{"1, 1", "1, 6", "8, 3", "3, 4", "5, 5", "8, 9"}, 17},
	}

	for idx, tc := range tt {
		t.Run(fmt.Sprintf("Case %d", idx), func(t *testing.T) {
			result, err := puzzle1(tc.lines)
			if err != nil {
				t.Fatalf("unexpected error: %+v", err)
			}
			if result != tc.result {
				t.Errorf("expected %d; got %d", tc.result, result)
			}
		})
	}
}

func TestPuzzle1Solution(t *testing.T) {
	expect := 3989

	lines := utils.ReadLines("input")
	result, err := puzzle1(lines)
	if err != nil {
		t.Fatalf("unexpected error: %+v", err)
	}
	if result != expect {
		t.Errorf("expected %d; got %d", expect, result)
	}
}

func TestPuzzle2(t *testing.T) {
	tt := []struct {
		lines  []string
		result int
	}{
		{[]string{"1, 1", "1, 6", "8, 3", "3, 4", "5, 5", "8, 9"}, 16},
	}

	for idx, tc := range tt {
		t.Run(fmt.Sprintf("Case %d", idx), func(t *testing.T) {
			result, err := puzzle2(tc.lines, 32)
			if err != nil {
				t.Fatalf("unexpected error: %+v", err)
			}
			if result != tc.result {
				t.Errorf("expected %d; got %d", tc.result, result)
			}
		})
	}
}

func TestPuzzle2Solution(t *testing.T) {
	expect := 49715

	lines := utils.ReadLines("input")
	result, err := puzzle2(lines, 10000)
	if err != nil {
		t.Fatalf("unexpected error: %+v", err)
	}
	if result != expect {
		t.Errorf("expected %d; got %d", expect, result)
	}
}
