package main

import (
	"flag"
	"fmt"

	"gitlab.com/rlbekkema/advent-of-code-2018/day01"
	"gitlab.com/rlbekkema/advent-of-code-2018/day02"
	"gitlab.com/rlbekkema/advent-of-code-2018/day03"
	"gitlab.com/rlbekkema/advent-of-code-2018/day04"
	"gitlab.com/rlbekkema/advent-of-code-2018/day05"
	"gitlab.com/rlbekkema/advent-of-code-2018/day06"
	"gitlab.com/rlbekkema/advent-of-code-2018/day07"
	"gitlab.com/rlbekkema/advent-of-code-2018/day08"
	"gitlab.com/rlbekkema/advent-of-code-2018/day09"
	"gitlab.com/rlbekkema/advent-of-code-2018/day10"
	"gitlab.com/rlbekkema/advent-of-code-2018/day11"
)

func main() {
	day := flag.Int("day", 1, "Puzzle day")
	flag.Parse()

	switch *day {
	case 1:
		day01.Puzzle1()
		day01.Puzzle2()
	case 2:
		day02.Puzzle1()
		day02.Puzzle2()
	case 3:
		squareInches, _ := day03.Puzzle1()
		fmt.Printf("Square inches in claims: %d\n", squareInches)

		claimID, err := day03.Puzzle2()
		if err != nil {
			panic(err)
		}
		fmt.Printf("Who the man, you the man, %d. Did you suspect it?\n", claimID)
	case 4:
		num, err := day04.Puzzle1()
		if err != nil {
			panic(err)
		}
		fmt.Printf("Result of puzzle 1: %d\n", num)
		num2, err := day04.Puzzle2()
		if err != nil {
			panic(err)
		}
		fmt.Printf("Result of puzzle 2: %d\n", num2)
	case 5:
		result, err := day05.Puzzle1()
		if err != nil {
			panic(err)
		}
		fmt.Printf("Remaining units: %d\n", result)
		result2, err := day05.Puzzle2()
		if err != nil {
			panic(err)
		}
		fmt.Printf("Shortest polymer: %d\n", result2)
	case 6:
		num, err := day06.Puzzle1()
		if err != nil {
			panic(err)
		}
		fmt.Printf("Largest finite area: %d\n", num)
		locsUnder10k, err := day06.Puzzle2()
		if err != nil {
			panic(err)
		}
		fmt.Printf("Locations under %d: %d\n", 10000, locsUnder10k)
	case 7:
		result := day07.Puzzle1()
		fmt.Printf("Step order: %s\n", result)
		result2 := day07.Puzzle2()
		fmt.Printf("Total time: %d\n", result2)
	case 8:
		result := day08.Puzzle1()
		fmt.Printf("Meta total: %d\n", result)
		result2 := day08.Puzzle2()
		fmt.Printf("Value of root node: %d\n", result2)
	case 9:
		result := day09.Puzzle1()
		fmt.Printf("Puzzle1 score: %d\n", result)
		result2 := day09.Puzzle2()
		fmt.Printf("Puzzle 2 score: %d\n", result2)
	case 10:
		day10.Puzzle1()
	case 11:
		fmt.Println(day11.Puzzle1())
		fmt.Println(day11.Puzzle2())
	default:
		fmt.Printf("Day %d does not exist\n", *day)
	}
}
