package day03

import (
	"bufio"
	"errors"
	"fmt"
	"os"

	"gitlab.com/rlbekkema/advent-of-code-2018/utils"
)

type Claim struct {
	ID         int
	X, Y, W, H int
}

func (c Claim) String() string {
	return fmt.Sprintf("ID: %d, X: %d, Y: %d, W: %d, H: %d", c.ID, c.X, c.Y, c.W, c.H)
}

func scan(rawClaim string) Claim {
	var claim Claim
	if _, err := fmt.Sscanf(rawClaim, "#%d @ %d,%d: %dx%d",
		&claim.ID, &claim.X, &claim.Y, &claim.W, &claim.H); err != nil {
		panic(err)
	}
	return claim
}

func readLines() []string {
	f, err := os.Open("day03/input")
	defer utils.MustClose(f)
	if err != nil {
		panic(err)
	}
	s := bufio.NewScanner(f)
	var lines []string
	for s.Scan() {
		lines = append(lines, s.Text())
	}
	return lines
}

func puzzle1(lines []string) (int, [1000][1000]uint8) {
	var grid [1000][1000]uint8
	squareInches := 0

	for _, rawClaim := range lines {
		claim := scan(rawClaim)

		for x := claim.X; x <= claim.X+claim.W-1; x++ {
			for y := claim.Y; y <= claim.Y+claim.H-1; y++ {
				grid[x][y]++
				if grid[x][y] == 2 {
					squareInches++
				}
			}
		}
	}
	return squareInches, grid
}

func Puzzle1() (int, [1000][1000]uint8) {
	lines := readLines()
	return puzzle1(lines)
}

func puzzle2(lines []string) (int, error) {
	_, grid := puzzle1(lines)

	allOnes := func(claim Claim) bool {
		for x := claim.X; x <= claim.X+claim.W-1; x++ {
			for y := claim.Y; y <= claim.Y+claim.H-1; y++ {
				if grid[x][y] != 1 {
					return false
				}
			}
		}
		return true
	}

	for _, rawClaim := range lines {
		claim := scan(rawClaim)
		if allOnes(claim) {
			return claim.ID, nil
		}
	}
	return 0, errors.New("no non-overlapping claim found")
}

func Puzzle2() (int, error) {
	lines := readLines()
	return puzzle2(lines)
}
