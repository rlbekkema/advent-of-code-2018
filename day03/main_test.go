package day03

import (
	"fmt"
	"testing"
)

func TestScan(t *testing.T) {
	tt := []struct {
		rawClaim string
		claim    Claim
	}{
		{rawClaim: "#1 @ 1,3: 4x4", claim: Claim{1, 1, 3, 4, 4}},
		{rawClaim: "#360 @ 942,333: 29x10", claim: Claim{360, 942, 333, 29, 10}},
		{rawClaim: "#565 @ 479,383: 19x11", claim: Claim{565, 479, 383, 19, 11}},
		{rawClaim: "#937 @ 726,449: 10x20", claim: Claim{937, 726, 449, 10, 20}},
	}

	for _, tc := range tt {
		t.Run(tc.rawClaim, func(t *testing.T) {
			claim := scan(tc.rawClaim)
			if claim != tc.claim {
				t.Errorf("expected claim to be %s; got %s", tc.claim, claim)
			}
		})
	}
}

func TestPuzzle1(t *testing.T) {
	tt := []struct {
		lines  []string
		result int
	}{
		{lines: []string{"#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2"}, result: 4},
		{lines: []string{"#1 @ 1,3: 4x4", "#3 @ 5,5: 2x2"}, result: 0},
	}

	for i, tc := range tt {
		t.Run(fmt.Sprintf("Case %d", i+1), func(t *testing.T) {
			result, _ := puzzle1(tc.lines)
			if result != tc.result {
				t.Errorf("expected result to be %d; got %d", tc.result, result)
			}
		})
	}
}

func TestPuzzle2(t *testing.T) {
	tt := []struct {
		lines  []string
		result int
	}{
		{lines: []string{"#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2"}, result: 3},
	}

	for i, tc := range tt {
		t.Run(fmt.Sprintf("Case %d", i+1), func(t *testing.T) {
			result, err := puzzle2(tc.lines)
			if err != nil {
				t.Errorf("unexpected error: %v", err)
			}
			if result != tc.result {
				t.Errorf("expected result to be %d; got %d", tc.result, result)
			}
		})
	}
}
