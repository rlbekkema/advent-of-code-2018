package utils

import (
	"bufio"
	"io"
	"io/ioutil"
	"os"
	"strings"
)

func MustClose(readCloser io.ReadCloser) {
	if err := readCloser.Close(); err != nil {
		panic(err)
	}
}

func ReadLines(file string) (lines []string) {
	f, err := os.Open(file)
	defer MustClose(f)
	if err != nil {
		panic(err)
	}
	s := bufio.NewScanner(f)
	for s.Scan() {
		lines = append(lines, s.Text())
	}
	return lines
}

func ReadAll(file string) string {
	f, err := os.Open(file)
	defer MustClose(f)
	if err != nil {
		panic(err)
	}
	data, err := ioutil.ReadAll(f)
	if err != nil {
		panic(err)
	}
	return strings.TrimSpace(string(data))
}
