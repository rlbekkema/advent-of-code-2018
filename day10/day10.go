package day10

import (
	"fmt"
	"strings"

	"gitlab.com/rlbekkema/advent-of-code-2018/utils"
)

type Point struct {
	x, y, dX, dY int
}

type Points []*Point

func (points Points) MinMax() (int, int, int, int) {
	var maxX, minX, maxY, minY int
	for idx, point := range points {
		if point.x > maxX || idx == 0 {
			maxX = point.x
		}
		if point.x < minX || idx == 0 {
			minX = point.x
		}
		if point.y > maxY || idx == 0 {
			maxY = point.y
		}
		if point.y < minY || idx == 0 {
			minY = point.y
		}
	}
	return maxX, minX, maxY, minY
}

func (points Points) Size() (int, int) {
	maxX, minX, maxY, minY := points.MinMax()
	width := minX*-1 + maxX
	height := minY*-1 + maxY
	return width, height
}

func (points Points) Step() {
	for _, point := range points {
		point.x += point.dX
		point.y += point.dY
	}
}

func (points Points) Render(step int) {
	maxX, minX, maxY, minY := points.MinMax()
	width := minX*-1 + maxX
	height := minY*-1 + maxY

	grid := make([][]bool, height+1)
	for _, point := range points {
		realX := point.x - minX
		realY := point.y - minY
		if grid[realY] == nil {
			grid[realY] = make([]bool, width+1)
		}
		grid[realY][realX] = true
	}

	for _, row := range grid {
		line := []rune(strings.Repeat(".", width+1))
		for x, val := range row {
			if val {
				line[x] = '#'
			}
		}
		fmt.Println(string(line))
	}

	fmt.Printf("Step: %d\n", step)
}

func puzzle1(lines []string) {
	var points Points = make([]*Point, len(lines))
	for idx, line := range lines {
		var point Point
		fmt.Sscanf(line, "position=<%d, %d> velocity=<%d, %d>", &point.x, &point.y, &point.dX, &point.dY)
		points[idx] = &point
	}

	foundLower := false
	step := 0
	for {
		step++
		points.Step()
		// width, height := points.Size()
		// if width < len(points) && height < len(points) {
		// 	fmt.Printf("Lower! %d, %d\n", width, height)
		// 	foundLower = true
		// 	points.Render(count)
		// 	continue
		// }

		if step == 10946 {
			points.Render(step)
			break
		}
		if foundLower {
			break
		}
	}
}

// 10946

func Puzzle1() {
	lines := utils.ReadLines("day10/input")
	puzzle1(lines)
}
