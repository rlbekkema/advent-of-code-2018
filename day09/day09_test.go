package day09

import (
	"fmt"
	"testing"
)

func TestPuzzle1(t *testing.T) {
	tt := []struct {
		players, lastMarble, expect int
	}{
		{players: 10, lastMarble: 1618, expect: 8317},
		{players: 13, lastMarble: 7999, expect: 146373},
		{players: 17, lastMarble: 1104, expect: 2764},
		{players: 21, lastMarble: 6111, expect: 54718},
		{players: 30, lastMarble: 5807, expect: 37305},
	}

	for idx, tc := range tt {
		res := puzzle1(tc.players, tc.lastMarble)
		if res != tc.expect {
			t.Fatalf("test %d: expected %d, got %d", idx+1, tc.expect, res)
		}
	}
}

func TestLoop(t *testing.T) {
	tt := []struct {
		leng, idx, expect int
	}{
		{leng: 10, idx: 15, expect: 5},
		{leng: 5, idx: 6, expect: 1},
		{leng: 5, idx: 5, expect: 0},
		{leng: 5, idx: 4, expect: 4},
		{leng: 5, idx: -3, expect: 2},
		{leng: 5, idx: -8, expect: 2},
		{leng: 5, idx: 11, expect: 1},
	}

	for idx, tc := range tt {
		res := loop(tc.leng, tc.idx)
		if res != tc.expect {
			t.Fatalf("test %d: expected %d, got %d", idx+1, tc.expect, res)
		}
	}
}

func ExamplePuzzle1() {
	res := Puzzle1()
	fmt.Printf("%d", res)

	// Output: 424639
}

func ExamplePuzzle2() {
	res := Puzzle2()
	fmt.Printf("%d", res)

	// Output: 3516007333
}
