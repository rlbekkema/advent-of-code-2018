package day09

func loop(leng, idx int) int {
	idx = idx % leng
	if idx >= leng {
		return idx - leng
	}
	if idx < 0 {
		return leng + idx
	}
	return idx
}

type List struct {
	first *Link
	last  *Link
}

func (l *List) Add(value int) *Link {
	add := &Link{
		list:  l,
		prev:  l.last,
		value: value,
	}
	l.last = add
	if l.first == nil {
		l.first = add
	}
	return add
}

type Link struct {
	list  *List
	next  *Link
	prev  *Link
	value int
}

func (l *Link) InsertAfter(value int) *Link {
	add := &Link{
		list:  l.list,
		next:  l.next,
		prev:  l,
		value: value,
	}
	if l.next == nil {
		l.list.last = add
	}
	if l.next != nil {
		l.next.prev = add
	}
	l.next = add
	return add
}

func (l *Link) Delete() {
	if l.next == nil {
		l.list.last = l.prev
	}
	if l.prev == nil {
		l.list.first = l.next
	}
	if l.next != nil {
		l.next.prev = l.prev
	}
	if l.prev != nil {
		l.prev.next = l.next
	}
}

func (l *Link) Loop(add int) *Link {
	if add >= 0 {
		for i := 0; i < add; i++ {
			if l.next == nil {
				l = l.list.first
				continue
			}
			l = l.next
		}
		return l
	}
	for i := 0; i > add; i-- {
		if l.prev == nil {
			l = l.list.last
			continue
		}
		l = l.prev
	}
	return l
}

func puzzle1(nrPlayers, maxMarble int) int {
	players := make([]int, nrPlayers)
	circle := &List{}
	current := circle.Add(0)
	player := 0

	for marble := 1; marble <= maxMarble; marble++ {
		if marble%23 == 0 {
			players[player] += marble
			link := current.Loop(-7)
			players[player] += link.value
			current = link.Loop(1)
			link.Delete()
		} else {
			link := current.Loop(1)
			current = link.InsertAfter(marble)
		}
		player = loop(nrPlayers, player+1)
	}

	var winScore int
	for _, score := range players {
		if score > winScore {
			winScore = score
		}
	}

	return winScore
}

func Puzzle1() int {
	return puzzle1(411, 71058)
}

func Puzzle2() int {
	return puzzle1(411, 7105800)
}
