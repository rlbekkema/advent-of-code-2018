package day11

import (
	"fmt"
	"testing"
)

func TestPowerLVL(t *testing.T) {
	tt := []struct {
		x, y, serial, expect int
	}{
		{x: 3, y: 5, serial: 8, expect: 4},
		{x: 122, y: 79, serial: 57, expect: -5},
		{x: 217, y: 196, serial: 39, expect: 0},
		{x: 101, y: 153, serial: 71, expect: 4},
	}

	for idx, tc := range tt {
		res := powerLVL(tc.x, tc.y, tc.serial)
		if res != tc.expect {
			t.Fatalf("test %d: expected %d, got %d", idx+1, tc.expect, res)
		}
	}
}

func TestPuzzle1(t *testing.T) {
	tt := []struct {
		serial           int
		expectX, expectY int
	}{
		{serial: 18, expectX: 33, expectY: 45},
		{serial: 42, expectX: 21, expectY: 61},
	}

	for idx, tc := range tt {
		resX, resY := puzzle1(tc.serial)
		if resX != tc.expectX {
			t.Fatalf("test %d: expected x to be %d, got %d", idx+1, tc.expectX, resX)
		}
		if resY != tc.expectY {
			t.Fatalf("test %d: expected y to be %d, got %d", idx+1, tc.expectY, resY)
		}
	}
}

func TestPuzzle2(t *testing.T) {
	tt := []struct {
		serial                 int
		expectX, expectY, size int
	}{
		{serial: 18, expectX: 90, expectY: 269, size: 16},
		{serial: 42, expectX: 232, expectY: 251, size: 12},
	}

	for idx, tc := range tt {
		resX, resY, size := puzzle2(tc.serial)
		if resX != tc.expectX || resY != tc.expectY || size != tc.size {
			t.Fatalf("test %d: expected %d,%d,%d, got %d,%d,%d", idx+1, tc.expectX, tc.expectY, tc.size, resX, resY, size)
		}
	}
}

// 234 108 16

func ExamplePuzzle1() {
	resX, resY := Puzzle1()
	fmt.Printf("%d,%d", resX, resY)

	// Output: 21,76
}

func ExamplePuzzle2() {
	resX, resY, resSize := Puzzle2()
	fmt.Printf("%d,%d,%d", resX, resY, resSize)

	// Output: 234,108,16
}
