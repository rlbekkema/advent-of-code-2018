package day11

func powerLVL(x, y, serial int) int {
	rackID := x + 10
	powerLVL := rackID*y + serial
	powerLVL *= rackID
	powerLVL = powerLVL % 1000 / 100
	return powerLVL - 5
}

func prepGrid(serial int) [90000]int {
	var myGrid [90000]int

	for idx := range myGrid {
		x := idx % 300
		y := idx / 300

		myGrid[idx] = powerLVL(x+1, y+1, serial)
	}

	return myGrid
}

// 4,5,10,20

// func prepGridPow(grid [90000]int, pow int) []int {
// 	if pow%90000 != 0 {
// 		panic("needs to be factor of 90000")
// 	}
// 	size := 90000 / pow
// 	newGrid := make([]int, size)
// 	for x := 0; x < size; x++ {

// 	}
// }

func puzzle1(serial int) (int, int) {
	grid := prepGrid(serial)

	var maxX, maxY, maxTotal int
	for idx := range grid {
		x := idx % 300
		y := idx / 300
		if x >= 298 || y >= 298 {
			continue
		}
		total := 0
		for dX := 0; dX < 3; dX++ {
			for dY := 0; dY < 3; dY++ {
				newIdx := ((y + dY) * 300) + (x + dX)

				total += grid[newIdx]
			}
		}

		if total > maxTotal {
			maxTotal = total
			maxX = x
			maxY = y
		}
	}

	return maxX + 1, maxY + 1
}

func puzzle2(serial int) (int, int, int) {
	grid := prepGrid(serial)

	type result struct {
		total, x, y, size int
	}
	var maxResult result

	for idx := range grid {
		x := idx % 300
		y := idx / 300
		total := 0 // grid[y*300+x]

		for size := 1; size <= 300; size++ {
			if x > (300-size) || y > (300-size) {
				continue
			}

			// Add new row to total
			rowY := y + (size - 1)
			for nSize := 0; nSize < size; nSize++ {
				rowX := x + nSize
				total += grid[rowY*300+rowX]
			}

			// Add new col to total
			colX := x + (size - 1)
			for nSize := 0; nSize < size-1; nSize++ {
				colY := y + nSize
				total += grid[colY*300+colX]
			}

			if total > maxResult.total {
				maxResult = result{
					total, x, y, size,
				}
			}
		}
	}

	return maxResult.x + 1, maxResult.y + 1, maxResult.size
}

func Puzzle1() (int, int) {
	return puzzle1(3031)
}

func Puzzle2() (int, int, int) {
	return puzzle2(3031)
}
