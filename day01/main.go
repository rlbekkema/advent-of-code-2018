package day01

import (
	"bufio"
	"fmt"
	"os"
)

func Puzzle1() {
	f, err := os.Open("day01/input")
	if err != nil {
		panic(err)
	}

	rdr := bufio.NewScanner(f)
	var total int
	for rdr.Scan() {
		var num int
		if _, err := fmt.Sscanf(rdr.Text(), "%d", &num); err != nil {
			panic(err)
		}
		total += num
	}
	if err := rdr.Err(); err != nil {
		panic(err)
	}
	fmt.Printf("Frequency: %d\n", total)
}

func Puzzle2() {
	f, err := os.Open("day01/input")
	if err != nil {
		panic(err)
	}

	var freq int
	freqs := map[int]bool{0: true}
	count := 1
outer:
	for {
		rdr := bufio.NewScanner(f)
		for rdr.Scan() {
			var num int
			txt := rdr.Text()
			if _, err := fmt.Sscanf(txt, "%d", &num); err != nil {
				panic(err)
			}

			//fmt.Printf("Current frequency %d, change of %s; resulting frequency %d.\n", freq, txt, freq+num)
			freq += num
			if freqs[freq] {
				// Second occurrence found
				fmt.Printf("First recurring frequency found: %d\n", freq)
				break outer
			}

			freqs[freq] = true
		}
		if err := rdr.Err(); err != nil {
			panic(err)
		}
		if _, err := f.Seek(0, 0); err != nil {
			panic(err)
		}
		count ++
	}
	fmt.Printf("Number of loops: %d\n", count)
}