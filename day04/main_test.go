package day04

import (
	"fmt"
	"testing"

	"gitlab.com/rlbekkema/advent-of-code-2018/utils"
)

func TestPuzzle1(t *testing.T) {
	tt := []struct {
		file   string
		result int
	}{
		{"input_test", 240},
		{"input", 73646},
	}

	for _, tc := range tt {
		t.Run(fmt.Sprintf("with file: %s", tc.file), func(t *testing.T) {
			result, err := puzzle1(utils.ReadLines(tc.file))
			if err != nil {
				t.Fatalf("something went wrong: %+v", err)
			}

			if result != tc.result {
				t.Errorf("expected result to be %d; got %d", tc.result, result)
			}
		})
	}
}

func TestPuzzle2(t *testing.T) {
	tt := []struct {
		file   string
		result int
	}{
		{"input_test", 4455},
		{"input", 4727},
	}

	for _, tc := range tt {
		t.Run(fmt.Sprintf("with file: %s", tc.file), func(t *testing.T) {
			result, err := puzzle2(utils.ReadLines(tc.file))
			if err != nil {
				t.Fatalf("something went wrong: %+v", err)
			}

			if result != tc.result {
				t.Errorf("expected result to be %d; got %d", tc.result, result)
			}
		})
	}
}
