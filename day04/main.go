package day04

import (
	"fmt"
	"sort"
	"strings"
	"time"

	"gitlab.com/rlbekkema/advent-of-code-2018/utils"
	"golang.org/x/xerrors"
)

type guard struct {
	id, sleptFor int
	sleepMap     [60]int
}

func sleepMaps(lines []string) (guards []guard, err error) {
	lineData := make(map[string][]string)
	guardData := make(map[string]int)
	for _, line := range lines {
		date := line[1:11]

		if strings.Contains(line, "Guard") {
			var guardID int
			if _, err := fmt.Sscanf(line[25:], "#%d", &guardID); err != nil {
				return nil, xerrors.Errorf("could not scan guard id: %w", err)
			}

			if line[12:14] == "23" {
				dt, err := time.Parse("[2006-01-02 15:04]", line[:18])
				if err != nil {
					return nil, xerrors.Errorf("could not parse date: %w", err)
				}
				date = dt.Add(time.Hour).Format("2006-01-02")
			}

			guardData[date] = guardID
		} else {
			lineData[date] = append(lineData[date], line)
		}
	}

	guardsMap := make(map[int]guard)
	for date, lines := range lineData {
		guardID := guardData[date]
		if guardID == 0 {
			return nil, xerrors.Errorf("no guard on this day: %q", date)
		}
		sort.Strings(lineData[date])
		guard := guardsMap[guardID]
		guard.id = guardID

		var rackTime int
		var fallAsleepTime time.Time
		for _, line := range lines {
			dt, err := time.Parse("[2006-01-02 15:04]", line[:18])
			if err != nil {
				return nil, xerrors.Errorf("could not parse date: %w", err)
			}
			if strings.Contains(line, "falls asleep") {
				fallAsleepTime = dt
			} else {
				min := int(dt.Sub(fallAsleepTime).Minutes())
				rackTime += min
				for i := fallAsleepTime.Minute(); i < dt.Minute(); i++ {
					guard.sleepMap[i]++
				}
			}
		}
		guard.sleptFor += rackTime
		guardsMap[guardID] = guard
	}

	// Pick guard who slept the most minutes
	var guardsArr []guard
	for _, guard := range guardsMap {
		guardsArr = append(guardsArr, guard)
	}

	return guardsArr, nil
}

func puzzle1(lines []string) (int, error) {
	guards, err := sleepMaps(lines)
	if err != nil {
		return 0, xerrors.Errorf("could not get sleep maps: %w", err)
	}

	sort.Slice(guards, func(i, j int) bool {
		return guards[i].sleptFor > guards[j].sleptFor
	})
	maxGuard := guards[0]
	// fmt.Println(maxGuard.sleepMap)

	// Pick minute at which guard slept the most times
	var maxMin, maxVal int
	for m, count := range maxGuard.sleepMap {
		// fmt.Printf("min: %d, times: %d\n", m, count)
		if count > maxVal {
			maxVal = count
			maxMin = m
		}
	}

	fmt.Printf("Guard who slept most: #%d, %d minutes\n", maxGuard.id, maxGuard.sleptFor)
	fmt.Printf("Slept most at 00:%d, %d times\n", maxMin, maxVal)

	result := maxGuard.id * maxMin
	return result, nil
}

func puzzle2(lines []string) (int, error) {
	guards, err := sleepMaps(lines)
	if err != nil {
		return 0, xerrors.Errorf("could not get sleep maps: %w", err)
	}

	// Of all guards, which guard is most frequently asleep on the same minute?
	var maxCount, maxMin int
	var maxGuard guard
	for _, guard := range guards {
		for min, count := range guard.sleepMap {
			if count > maxCount {
				maxCount = count
				maxMin = min
				maxGuard = guard
			}
		}
	}

	fmt.Printf("Most slept minute by guard #%d, minute %d, count %d\n", maxGuard.id, maxMin, maxCount)

	return maxGuard.id * maxMin, nil
}

func Puzzle1() (int, error) {
	lines := utils.ReadLines("day04/input")
	return puzzle1(lines)
}

func Puzzle2() (int, error) {
	lines := utils.ReadLines("day04/input")
	return puzzle2(lines)
}
