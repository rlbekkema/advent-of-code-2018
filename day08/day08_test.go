package day08

import (
	"fmt"
	"testing"

	"gitlab.com/rlbekkema/advent-of-code-2018/utils"
)

func TestTokenizer(t *testing.T) {
	tt := []struct {
		input  string
		output []int
	}{
		{input: "1 2 3", output: []int{1, 2, 3}},
		{input: "2 3\n", output: []int{2, 3}},
		{input: "4 11 99 2", output: []int{4, 11, 99, 2}},
		{input: "", output: []int{}},
	}

	for idx, tc := range tt {
		tzer := NewTokenizer(tc.input)
		for odx, expect := range tc.output {
			num := tzer.Next()
			if num == -1 {
				t.Fatalf("expected number, got EOF")
			}
			if num != expect {
				t.Fatalf("test %d, num %d: expected %d, got %d", idx+1, odx+1, expect, num)
			}
		}
	}
}

func TestPuzzle1(t *testing.T) {
	tt := []struct {
		input  string
		expect int
	}{
		{"2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2", 138},
		{"1 1 0 1 2 3", 5},
	}

	for idx, tc := range tt {
		res := puzzle1(tc.input)
		if res != tc.expect {
			t.Fatalf("test %d: expected %d, got %d", idx+1, tc.expect, res)
		}
	}
}

func ExamplePuzzle1() {
	data := utils.ReadAll("./input")
	fmt.Println(puzzle1(data))

	// Output: 36307
}

func TestNodeValue(t *testing.T) {
	tt := []struct {
		node   Node
		expect int
	}{
		{node: Node{meta: []int{1, 2, 3}}, expect: 6},
		{
			node: Node{meta: []int{1, 2, 3}, children: []Node{
				{meta: []int{2, 3}},
				{meta: []int{5}},
			}},
			expect: 10,
		},
		{
			node: Node{meta: []int{1, 3}, children: []Node{
				{meta: []int{2, 3}},
				{meta: []int{5}},
				{meta: []int{2}, children: []Node{
					{meta: []int{1}},
					{meta: []int{3, 4}},
				}},
			}},
			expect: 12,
		},
	}

	for idx, tc := range tt {
		val := tc.node.Value()
		if val != tc.expect {
			t.Fatalf("test %d: expected %d, got %d", idx+1, tc.expect, val)
		}
	}
}

func ExamplePuzzle2() {
	data := utils.ReadAll("./input")
	fmt.Println(puzzle2(data))

	// Output: 25154
}
