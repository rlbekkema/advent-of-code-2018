package day08

import (
	"strconv"
	"strings"

	"gitlab.com/rlbekkema/advent-of-code-2018/utils"
)

type Tokenizer struct {
	input []string
	pos   int
}

func NewTokenizer(input string) *Tokenizer {
	return &Tokenizer{input: strings.Split(strings.TrimSpace(input), " ")}
}

func (t *Tokenizer) Next() int {
	if t.pos < len(t.input) {
		num, err := strconv.Atoi(t.input[t.pos])
		if err != nil {
			return -1
		}
		t.pos++
		return num
	}
	return -1
}

type Node struct {
	children []Node
	meta     []int
}

func (n Node) Value() int {
	val := 0
	// If there a no children, value is sum of metas
	if len(n.children) == 0 {
		for _, meta := range n.meta {
			val += meta
		}
		return val
	}

	// Otherwise, metas are indeces for children whose value to add
	for _, meta := range n.meta {
		if meta := meta - 1; meta < len(n.children) {
			val += n.children[meta].Value()
		}
	}
	return val
}

func (t *Tokenizer) ReadNodes(nr int) ([]Node, int) {
	nodes := make([]Node, nr)
	metaTotal := 0
	for i := 0; i < nr; i++ {
		nrChildren := t.Next()
		nrMeta := t.Next()
		children, newMetaTotal := t.ReadNodes(nrChildren)
		metaTotal += newMetaTotal
		meta := make([]int, nrMeta)
		for j := 0; j < nrMeta; j++ {
			meta[j] = t.Next()
			metaTotal += meta[j]
		}
		nodes[i] = Node{children, meta}
	}
	return nodes, metaTotal
}

func puzzle1(input string) int {
	tzer := NewTokenizer(input)
	root, meta := tzer.ReadNodes(1)

	_ = root

	return meta
}

func Puzzle1() int {
	data := utils.ReadAll("./day08/input")
	return puzzle1(data)
}

func puzzle2(input string) int {
	tzer := NewTokenizer(input)
	nodes, _ := tzer.ReadNodes(1)

	return nodes[0].Value()
}

func Puzzle2() int {
	data := utils.ReadAll("./day08/input")
	return puzzle2(data)
}
