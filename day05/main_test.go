package day05

import (
	"fmt"
	"io"
	"testing"

	"gitlab.com/rlbekkema/advent-of-code-2018/utils"
)

func closer(readCloser io.ReadCloser) {
	if err := readCloser.Close(); err != nil {
		panic(err)
	}
}

func TestPuzzle1(t *testing.T) {
	tt := []struct {
		polymer string
		len     int
	}{
		{"aA", 0},
		{"cBaAbc", 2},
		{"dabAcCaCBAcCcaDA", 10},
		{utils.ReadAll("input"), 11590},
	}

	for i, tc := range tt {
		t.Run(fmt.Sprintf("Case %d", i), func(t *testing.T) {
			result, err := puzzle1([]rune(tc.polymer))
			if err != nil {
				t.Fatalf("unexpected error: %v", err)
			}
			if len(result) != tc.len {
				t.Fatalf("wrong length, expected %v; got %v", tc.len, len(result))
			}
		})
	}
}

func TestPuzzle2(t *testing.T) {
	tt := []struct {
		polymer string
		result  int
	}{
		{"dabAcCaCBAcCcaDA", 4},
	}

	for i, tc := range tt {
		t.Run(fmt.Sprintf("Case %d", i), func(t *testing.T) {
			result, err := puzzle2(tc.polymer)
			if err != nil {
				t.Fatalf("unexpected error: %v", err)
			}
			if result != tc.result {
				t.Fatalf("wrong length, expected %v; got %v", tc.result, result)
			}
		})
	}
}
