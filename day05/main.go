package day05

import (
	"fmt"
	"strings"
	"unicode"

	"gitlab.com/rlbekkema/advent-of-code-2018/utils"
)

func puzzle1(polymer []rune) ([]rune, error) {
	// If called with empty array, there is nothing to do.
	if len(polymer) == 0 {
		return polymer, nil
	}

	for idx := range polymer {
		r1 := polymer[idx]
		r2 := polymer[idx+1]

		// Find same letter but different case
		if unicode.ToUpper(r1) == unicode.ToUpper(r2) && unicode.IsUpper(r1) != unicode.IsUpper(r2) {
			// Remove both from polymer
			fromIdx := idx - 3
			if fromIdx < 0 {
				fromIdx = 0
			}
			toIdx := idx + 5
			if toIdx > len(polymer)-1 {
				toIdx = len(polymer) - 1
			}
			polymer = append(polymer[:idx], polymer[idx+2:]...)
			break
		}

		// Last char reached, nothing more to do
		if idx == len(polymer)-2 {
			return polymer, nil
		}
	}
	return puzzle1(polymer)
}

func Puzzle1() (int, error) {
	polymer := utils.ReadAll("day05/input")
	poly, err := puzzle1([]rune(polymer))
	if err != nil {
		panic(err)
	}

	return len(poly), nil
}

func puzzle2(polymer string) (int, error) {
	minCount := -1
	var minChar rune
	for char := 'a'; char <= 'z'; char++ {
		r := strings.NewReplacer(string(unicode.ToLower(char)), "", string(unicode.ToUpper(char)), "")
		polymer := r.Replace(polymer)
		poly, err := puzzle1([]rune(polymer))
		if err != nil {
			panic(err)
		}
		if len(poly) < minCount || minCount == -1 {
			minCount = len(poly)
			minChar = char
		}
		fmt.Printf("With stripping letter %s, length is %d\n", string(char), len(poly))
	}
	fmt.Printf("Shortest polymer is with letter %s, at %d\n", string(minChar), minCount)
	return minCount, nil
}

func Puzzle2() (int, error) {
	polymer := utils.ReadAll("day05/input")
	return puzzle2(polymer)
}
