package day02

import (
	"bufio"
	"fmt"
	"os"

	"gitlab.com/rlbekkema/advent-of-code-2018/utils"
)

func loadFile() *os.File {
	f, err := os.Open("day02/input")
	if err != nil {
		panic(err)
	}
	return f
}

func Puzzle1() {
	f := loadFile()
	defer utils.MustClose(f)
	s := bufio.NewScanner(f)
	var doubs int
	var trips int
	for s.Scan() {
		code := s.Text()
		counts := map[rune]int{}
		for _, char := range code {
			counts[char] += 1
		}
		haveTwo := false
		haveThree := false
		for _, count := range counts {
			if !haveTwo && count == 2 {
				doubs++
				haveTwo = true
			}
			if !haveThree && count == 3 {
				trips++
				haveThree = true
			}
		}
	}
	if err := s.Err(); err != nil {
		panic(err)
	}

	fmt.Printf("Twos: %d, Threes: %d, Checksum: %d\n", doubs, trips, doubs*trips)
}

func Puzzle2() {
	f := loadFile()
	defer utils.MustClose(f)
	s := bufio.NewScanner(f)
	var lines []string
	for s.Scan() {
		lines = append(lines, s.Text())
	}

	compare := func(str1, str2 string) string {
		diffs := false
		str2runes := []rune(str2)
		var i int
		for idx, char := range str1 {
			if char != str2runes[idx] {
				if diffs {
					return ""
				}
				i = idx
				diffs = true
			}
		}
		a := append(str2runes[:i], str2runes[i+1:]...)
		return string(a)
	}

	for idx, line1 := range lines {
		for _, line2 := range lines[idx+1:] {
			if str := compare(line1, line2); str != "" {
				fmt.Printf("Found them: %s, %s (common: %s)\n", line1, line2, str)
			}
		}
	}
}
